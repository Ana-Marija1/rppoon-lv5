﻿using System;

namespace Rppoon_LV5_Z5
{
    class Program
    {
        static void Main(string[] args)
        { 
            ReminderNote reminderNote = new ReminderNote("Complete the task.",new PurpleTheme());
            reminderNote.Show();
            ReminderNote secondreminder = new ReminderNote("Clean the room.", new LightTheme());
            secondreminder.Show();
        }
    }
}
