﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV5_Z6
{
    class PurpleTheme:ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.DarkMagenta;
        }
        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.White;
        }
        public string GetHeader(int width)
        {
            return new string('+', width);
        }
        public string GetFooter(int width)
        {
            return new string('+', width);
        }
    }
}
