﻿using System;

namespace Rppoon_LV5_Z6
{
    class Program
    {
        static void Main(string[] args)
        {
            GroupNote groupNote1 = new GroupNote("Complete all the tasks.", new LightTheme());
            groupNote1.AddUser("Ana");
            groupNote1.AddUser("Marija");
            groupNote1.AddUser("Pero");
            groupNote1.Show();

            GroupNote groupNote2 = new GroupNote("Clean your rooms.", new PurpleTheme());
            groupNote2.AddUser("Lana");
            groupNote2.AddUser("Marko");
            groupNote2.AddUser("Robi");
            groupNote2.RemoveUser("Robi");
            groupNote2.Show();
        }

    }
}
