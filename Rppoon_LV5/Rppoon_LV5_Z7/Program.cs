﻿using System;

namespace Rppoon_LV5_Z7
{
    class Program
    {
        static void Main(string[] args)
        {
            ReminderNote reminderNote = new ReminderNote("Complete the task.", new PurpleTheme());
            ReminderNote secondreminderNote = new ReminderNote("Clean the room.", new LightTheme());
            
            GroupNote groupNote1 = new GroupNote("Complete all the tasks.", new LightTheme());
            groupNote1.AddUser("Ana");
            groupNote1.AddUser("Marija");
            groupNote1.AddUser("Pero");

            GroupNote groupNote2 = new GroupNote("Clean your rooms.", new PurpleTheme());
            groupNote2.AddUser("Lana");
            groupNote2.AddUser("Marko");
            groupNote2.AddUser("Robi");
            groupNote2.RemoveUser("Robi");

            Notebook notebook1 = new Notebook();
            notebook1.AddNote(reminderNote);
            notebook1.AddNote(secondreminderNote);
            notebook1.AddNote(groupNote1);
            notebook1.AddNote(groupNote2);
            //Testing original Notebook class:
            //notebook1.Display();
            notebook1.ChangeTheme(new PurpleTheme());
            //notebook1.Display();

            Notebook notebook2 = new Notebook(new LightTheme());
            notebook2.AddNewTheme(reminderNote);
            notebook2.AddNewTheme(secondreminderNote);
            notebook2.AddNewTheme(groupNote1);
            notebook2.AddNewTheme(groupNote2);
            notebook2.Display();
        }
    }

}
