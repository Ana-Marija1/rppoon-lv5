﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV5_Z7
{
    class GroupNote:Note
    {
        private List<string> users;

        public GroupNote(string message, ITheme theme) : base(message, theme)
        {
            users = new List<string>();
        }
        public void AddUser(string user)
        {
            this.users.Add(user);
        }

        public void RemoveUser(string user)
        {
            this.users.Remove(user);
        }

        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("GROUP REMINDER: ");
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);
            foreach (string user in users)
            {
                Console.WriteLine(user);
            }
            Console.WriteLine();
            Console.ResetColor();
        }
    }
}
