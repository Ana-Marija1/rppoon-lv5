﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV5_Z7
{
    interface ITheme
    {
        void SetBackgroundColor();
        void SetFontColor();
        string GetHeader(int width);
        string GetFooter(int width);
    }
}
