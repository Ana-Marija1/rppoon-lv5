﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV5_Z2
{
    class ShippingService
    {
        public double UnitPrice { private get; set; }
        public ShippingService()
        {
            this.UnitPrice = 4.50;
        }
        public ShippingService(double unitPrice)
        {
            this.UnitPrice = unitPrice;
        }
        public double GetUnitPrice(IShipable shipable)
        {
            return shipable.Weight*this.UnitPrice;
        }
    }
}
