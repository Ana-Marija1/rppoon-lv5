﻿using System;
using System.Collections.Generic;

namespace Rppoon_LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 1.
            //U primjeru 1 zamijeniti sučelje IBillable sučeljem IShipable danim u nastavku te napraviti odgovarajuće
            //dopune klasa koje ga ugrađuju.

            Product toiletPaper = new Product("Soft Toilet Paper, 12 Rolls", 0.5, 29.10);
            Product toothpaste = new Product("Whitening Toothpaste", 0.1, 17.50);
            Box hygieneSupplies = new Box("Hygiene Supplies Box");
            hygieneSupplies.Add(toiletPaper);
            hygieneSupplies.Add(toothpaste);

            Product milk = new Product("Semi Skimmed Milk", 2.272, 9.50);
            Product flour = new Product("White Bread Flour", 1.5,9.99);
            Product noodles = new Product("Ramen Chicken Noodles", 0.1, 3.50);
            Product beans = new Product("Baked Beans In Tomato Sauce", 0.415, 7.50);
            
            Box groceries = new Box("Groceries Box");
            groceries.Add(milk);
            groceries.Add(flour);
            groceries.Add(noodles);
            groceries.Add(new Product("Greek Style Yogurt", 0.5, 5.99));
            groceries.Add(beans);
            groceries.Remove(milk);

            Box purchase = new Box("Purchase Box");
            purchase.Add(hygieneSupplies);
            purchase.Add(groceries);

            Console.WriteLine(purchase.Description());
            Console.WriteLine("Total wight: {0} kg\nTotal price: {1:C}", Math.Round(purchase.Weight,3), purchase.Price);

        }
    }
}
