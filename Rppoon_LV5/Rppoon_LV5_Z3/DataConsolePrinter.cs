﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Rppoon_LV5_Z3
{
    class DataConsolePrinter
    {
        public void Print(IDataset dataset)
        {
            if (dataset.GetData() == null)
            {
                Console.WriteLine("Dataset is null.\n");
                return;
            }
            
            foreach(List<string> data in dataset.GetData())
            {
                foreach(string item in data)
                {
                    Console.Write(item+" ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
