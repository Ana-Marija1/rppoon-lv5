﻿using System;

namespace Rppoon_LV5_Z3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 3.
            //Testirati proxy-ije iz primjera 2.2 i 2.3.Za potrebe ispisa podataka napisati klasu DataConsolePrinter koja ima
            //odgovarajuću metodu za ispis. Također, za potrebe testiranja može se koristiti primjer CSV datoteke dan na
            //kraju ovog poglavlja.

            string filePath = "D:\\Ana-Marija\\Faks\\IV. semestar\\RPPOON\\Rppoon_LV5\\Rppoon_LV5_Z3\\CSVFile.txt";
            IDataset virtualProxyDataset = new VirtualProxyDataset(filePath);
            User user1 = User.GenerateUser("Ana");
            User user2 = User.GenerateUser("Marija");
            IDataset protectionProxyDataset1 = new ProtectionProxyDataset(user1);

            DataConsolePrinter printer = new DataConsolePrinter();

            Console.WriteLine("Virtual proxy dataset for user ID = 1:");
            printer.Print(virtualProxyDataset);
            Console.WriteLine("Protection proxy dataset for user ID = 1:");
            printer.Print(protectionProxyDataset1);

            IDataset protectionProxyDataset2 = new ProtectionProxyDataset(user2);

            Console.WriteLine("Virtual proxy dataset for user ID = 2:");
            printer.Print(virtualProxyDataset);
            Console.WriteLine("Protection proxy dataset for user ID = 2:");
            printer.Print(protectionProxyDataset2);
        }
    }
}
