﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Rppoon_LV5_Z3
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();

    }
}
