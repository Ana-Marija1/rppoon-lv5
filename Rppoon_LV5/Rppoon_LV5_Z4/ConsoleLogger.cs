﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV5_Z4
{
    class ConsoleLogger
    {
        public static ConsoleLogger instance;
        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
            {
                instance = new ConsoleLogger();
            }
            return instance;
        }
        public void Log(string message)
        {
            Console.WriteLine(message);
            Console.WriteLine(DateTime.Now);
            Console.WriteLine();
        }
    }
}
