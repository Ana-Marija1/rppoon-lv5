﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Rppoon_LV5_Z4
{
    class DataConsolePrinter
    {
        public void Print(IDataset dataset)
        {
            ReadOnlyCollection<List<string>> collection = dataset.GetData();

            if (collection == null)
            {
                Console.WriteLine("Dataset is null.\n");
                return;
            }

            foreach (List<string> data in collection)
            {
                foreach (string item in data)
                {
                    Console.Write(item + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
