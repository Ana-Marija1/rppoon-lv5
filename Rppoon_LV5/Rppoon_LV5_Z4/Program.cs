﻿using System;

namespace Rppoon_LV5_Z4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 4.
            //Napisati proxy za logiranje dohvaćanja podataka objekta klase Dataset.Za potrebe logiranja napisati
            //singleton ConsoleLogger i koristiti ga u proxy - iju.Bilježiti uz odgovarajuću poruku i vrijeme pristupa. Za
            // potrebe testiranja može se iskoristiti primjer CSV datoteke u nastavku.

            string filePath = "D:\\Ana-Marija\\Faks\\IV. semestar\\RPPOON\\Rppoon_LV5\\Rppoon_LV5_Z3\\CSVFile.txt";
            IDataset virtualProxyDataset1 = new VirtualProxyDataset(filePath);
            IDataset virtualProxyDataset2 = new VirtualProxyDataset(filePath);
            User user1 = User.GenerateUser("Ana");
            User user2 = User.GenerateUser("Marija");
            IDataset protectionProxyDataset1 = new ProtectionProxyDataset(user1);
            IDataset protectionProxyDataset2 = new ProtectionProxyDataset(user2);
            IDataset loggingProxyDataset1 = new LoggingProxyDataset(filePath);
            IDataset loggingProxyDataset2 = new LoggingProxyDataset(filePath);

            DataConsolePrinter printer = new DataConsolePrinter();

            Console.WriteLine("Virtual proxy dataset for user ID = 1:\n");
            printer.Print(virtualProxyDataset1);
            Console.WriteLine("Protection proxy dataset for user ID = 1:\n");
            printer.Print(protectionProxyDataset1);
            Console.WriteLine("Logging proxy dataset for user ID = 1:\n");
            printer.Print(loggingProxyDataset1);

            Console.WriteLine("Virtual proxy dataset for user ID = 2:\n");
            printer.Print(virtualProxyDataset2);
            Console.WriteLine("Protection proxy dataset for user ID = 2:\n");
            printer.Print(protectionProxyDataset2);
            Console.WriteLine("Logging proxy dataset for user ID = 2:\n");
            printer.Print(loggingProxyDataset2);

        }
    }
}
