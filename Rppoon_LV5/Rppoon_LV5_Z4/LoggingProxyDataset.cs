﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Rppoon_LV5_Z4
{
    class LoggingProxyDataset:IDataset
    {
        private string filePath;
        private Dataset dataset;
        public LoggingProxyDataset(string filePath)
        {
            this.filePath = filePath;
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            ConsoleLogger.GetInstance().Log("Access Time:");
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            
            return dataset.GetData();
        }
    }
}
